
// --------------------------------------------- //
// ------- Random Game Kristofer Brink --------- //
// -------- Created by Nikhil Suresh ----------- //
// -------- Three.JS is by Mr. doob  ----------- //
// --------------------------------------------- //

// ------------------------------------- //
// ------- GLOBAL VARIABLES ------------ //
// ------------------------------------- //

// scene object variables
var renderer, scene, camera, pointLight, spotLight;

// field variables
var fieldWidth = 400, fieldHeight = 200;



// game-related variables
var score1 = 0, score2 = 0;
// you can change this to any positive whole number
var maxScore = 7;

// set opponent reflexes (0 - easiest, 1 - hardest)
var difficulty = 0.2;

// ------------------------------------- //
// ------- GAME FUNCTIONS -------------- //
// ------------------------------------- //

function setup()
{
	// Set up physics
	Physijs.scripts.worker = 'Scripts/physijs_worker.js';
    Physijs.scripts.ammo = 'ammo.js';
	
	// update the board to reflect the max score for match win
	document.getElementById("winnerBoard").innerHTML = "First to " + maxScore + " wins!";
	
	// now reset player and opponent scores
	score1 = 0;
	score2 = 0;
	
	// set up all the 3D objects in the scene	
	createScene();
	
	// and let's get cracking!
	draw();
}

function createScene()
{
	// set the scene size
	var WIDTH = window.innerWidth;
	var HEIGHT = window.innerHeight;

	// set some camera attributes
	var VIEW_ANGLE = 50,
	  ASPECT = WIDTH / HEIGHT,
	  NEAR = 0.1,
	  FAR = 10000;

	var c = document.getElementById("gameCanvas");

	// create a WebGL renderer, camera
	// and a scene
	renderer = new THREE.WebGLRenderer({antialias: true});
	camera =
	  new THREE.PerspectiveCamera(
		VIEW_ANGLE,
		ASPECT,
		NEAR,
		FAR);

	scene = new Physijs.Scene();

	// add the camera to the scene
	scene.add(camera);
	
	// set a default position for the camera
	// not doing this somehow messes up shadow rendering
	camera.position.z = 200;
	
	// start the renderer
	renderer.setSize(WIDTH, HEIGHT);

	// attach the render-supplied DOM element
	c.appendChild(renderer.domElement);

    
    // ------------------------------------------- Set Gravity
    scene.setGravity(new THREE.Vector3( 0, -1000, 0 ));
		
	
    // ------------------------------------------- Create material 
	// create the table's material
	var tableMaterial =
	  new THREE.MeshLambertMaterial(
		{
		  color: 0x111111
		});
	// create the pillar's material
	var pillarMaterial =
	  new THREE.MeshLambertMaterial(
		{
		  color: 0x534d0d
		});
	// create the ground's material
	var groundMaterial =
	  new THREE.MeshLambertMaterial(
		{
		  color: 0x888888
		});
    // ------------------------------------------- ^
		

	
	
	// // set up the sphere vars
	// lower 'segment' and 'ring' values will increase performance
	var radius = 10,
		segments = 10,
		rings = 16;
		
	// // create the sphere's material
	var sphereMaterial =
	  new THREE.MeshLambertMaterial(
		{
		  color: 0xD43001
		});
		
	// Create a ball with sphere geometry
	ball = new Physijs.SphereMesh(

	  new THREE.SphereGeometry(
		radius,
		segments,
		rings),

	  sphereMaterial, 1);

	// // add the sphere to the scene
	scene.add(ball);
	
	ball.position.x = 0;
	ball.position.y = 0;
	// set ball above the table surface
	ball.position.z = 4 * radius;
	ball.receiveShadow = true;
    ball.castShadow = true;
	
	
	// we iterate 10x (5x each side) to create pillars to show off shadows
	// this is for the pillars on the right
    for (i = 0; i < 5; i++){
		var backdrop = new Physijs.BoxMesh(

		  new THREE.CubeGeometry( 
		  1000, 
		  100, 
		  500, 
		  1, 
		  1,
		  1 ),

		  pillarMaterial, 0);
		  
		backdrop.position.x = -50 + 1 * 100;
		backdrop.position.y = -230 + i * 100;
		backdrop.position.z = -30 - i * 500;
		backdrop.castShadow = true;
		backdrop.receiveShadow = true;		
		scene.add(backdrop);	
    }
	
	
	// // create a point light
	pointLight =
	  new THREE.PointLight(0xF8D898);

	// set its position
	pointLight.position.x = 250;
	pointLight.position.y = 200;
	pointLight.position.z =  200;
	pointLight.intensity = 2.9;
	pointLight.distance = 10000;
	// add to the scene
	scene.add(pointLight);
		
	// add a spot light
	// this is important for casting shadows
    spotLight = new THREE.SpotLight(0xF8D898);
    spotLight.position.set(0, 0, 460);
    spotLight.intensity = 1.5;
    spotLight.castShadow = true;
    scene.add(spotLight);
	
	// MAGIC SHADOW CREATOR DELUXE EDITION with Lights PackTM DLC
	renderer.shadowMapEnabled = true;		
}

function draw()
{	
	// Physijs simulate
	scene.simulate(); // run physics
	// draw THREE.JS scene
	renderer.render(scene, camera);
	// loop draw function call
	requestAnimationFrame(draw);
	
	ballPhysics();
	cameraPhysics();
	playerBallMovement();
}

function ballPhysics()
{
	
}




// Handles player's movement
function playerBallMovement()
{
	
	// move left
	if (Key.isDown(Key.A))		
	{
		// Move left
		ball.applyCentralForce(ball.matrix.multiplyVector4(new THREE.Vector3(-1000, 0, 0)));
	}	
	// move right
	else if (Key.isDown(Key.D))
	{
		ball.applyCentralForce(ball.matrix.multiplyVector4(new THREE.Vector3(1000, 0, 0)));
	}
    else if (Key.isDown(Key.W))
    {
        ball.applyCentralForce(ball.matrix.multiplyVector4(new THREE.Vector3(0, 0, -1000)))
    }
     else if (Key.isDown(Key.S))
    {
        ball.applyCentralForce(ball.matrix.multiplyVector4(new THREE.Vector3(0, 0, 1000)))
    }
    else if(Key.isDown(Key.SPACE)) {
        ball.applyCentralForce(ball.matrix.multiplyVector4(new THREE.Vector3(0,2000,0)))
    }
	// else don't move paddle
	else
	{
		
	}
	/*
	paddle1.scale.y += (1 - paddle1.scale.y) * 0.2;	
	paddle1.scale.z += (1 - paddle1.scale.z) * 0.2;	
	paddle1.position.y += paddle1DirY;
	*/
	
}

// Handles camera and lighting logic
function cameraPhysics()
{
	// we can easily notice shadows if we dynamically move lights during the game
	spotLight.position.x = ball.position.x;
    spotLight.position.y = ball.position.y + 30;
	spotLight.position.z = ball.position.z;
	
	// move to behind the player's paddle
	camera.position.x = ball.position.x;
	camera.position.y = ball.position.y + 400;
	camera.position.z = ball.position.z + 500;
	
	// rotate to face towards the opponent
	camera.rotation.x = -30 * Math.PI/180;
	//camera.rotation.y = -60 * Math.PI/180;
	//camera.rotation.z = -90 * Math.PI/180;
}

